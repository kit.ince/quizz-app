/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		"./src/**/*.vue",
		"./src/**/*.js",
		"./*.html",
		"./resources/**/*.blade.php",
		"./resources/**/*.js",
		"./resources/**/*.vue",
	],
	theme: {
		extend: {},
	},
	plugins: [],
};
